package util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.message.MessageI;

public class GsonSingleton {

	private static Gson gson;

	public static Gson getInstance(){
		if (gson == null){
			gson = new GsonBuilder()
					.excludeFieldsWithoutExposeAnnotation()
					.registerTypeAdapter(MessageI.class, new MessageGsonAdapter())
					.create();
		}
		return gson;
	}
	
}