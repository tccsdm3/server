package controller;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import model.Call;
import model.Client;
import model.message.Message;
import model.message.ServerMessage;
import model.type.ServerMessageType;
import service.ClientConnectionService;

public class SocketController implements Observer{

	private ServerSocket serverSocket;
	
	private ClientConnectionService clientConnectionService;
	
	private List<Client> clientsWithOutCall = new ArrayList<Client>();
	
	private List<Call> calls = new ArrayList<Call>();
	
	public SocketController() {
		try {
			serverSocket = new ServerSocket(2018);
			clientConnectionService = new ClientConnectionService(serverSocket);
			clientConnectionService.addObserver(this);
			(new Thread(clientConnectionService)).start();
		} catch (IOException e) {e.printStackTrace();}
	}

	public void update(Observable observable, Object object) {
		if (observable instanceof ClientConnectionService){
			Client client = new Client(getNextId(), (Socket)object);
			client.addObserver(this);
			
			clientsWithOutCall.add(client);
			
			for (Client clientAux : clientsWithOutCall)
				sendMessageToOne(new ServerMessage(ServerMessageType.ADD_USER, clientAux), client);
			
			sendMessageToAll(new ServerMessage(ServerMessageType.ADD_USER, client));

			System.out.println("New client connected : "+client.getName());
			System.out.println("Clients : "+clientsWithOutCall.toString());
		}
		else if (observable instanceof Client){
			Client client = (Client)observable;
			Client clientAux = null;
			Message message = (Message) object;
			switch (message.getMessageType()) {
			case CHANGE_NAME:
				sendMessageToAll(new ServerMessage(ServerMessageType.CHANGE_NAME, client));
				System.out.println("Client "+client.getId()+" changed name to : "+client.getName());
				break;
			case EXIT:
				sendMessageToAll(new ServerMessage(ServerMessageType.DEL_USER, client));
				System.out.println("Client : "+client.getName()+" removed");
				removeClient(client);
				break;
			case CALL_USER:
				clientAux = new Client(Integer.valueOf(message.getValue()));
				clientAux = clientsWithOutCall.get(clientsWithOutCall.indexOf(clientAux));
				sendMessageToOne(new ServerMessage(ServerMessageType.USER_CALLING, client), clientAux);
				System.out.println("Client : "+client.getName()+" called : "+clientAux.getName());
				break;
			case CANCEL_CALL_USER:
				clientAux = new Client(Integer.valueOf(message.getValue()));
				clientAux = clientsWithOutCall.get(clientsWithOutCall.indexOf(clientAux));
				sendMessageToOne(new ServerMessage(ServerMessageType.USER_CANCEL_CALLING, client), clientAux);
				System.out.println("Client : "+client.getName()+" cancel the call to : "+clientAux.getName());
				break;
			case ACCEPT_CALL:
				clientAux = new Client(Integer.valueOf(message.getValue()));
				clientAux = clientsWithOutCall.get(clientsWithOutCall.indexOf(clientAux));
				createCall(client, clientAux);
				sendMessageToOne(new ServerMessage(ServerMessageType.USER_ACCEPT, client), clientAux);
				System.out.println("Client : "+client.getName()+" accpet : "+clientAux.getName());
				System.out.println("Calls : "+calls.toString());
				break;
			case DENY_CALL:
				clientAux = new Client(Integer.valueOf(message.getValue()));
				clientAux = clientsWithOutCall.get(clientsWithOutCall.indexOf(clientAux));
				sendMessageToOne(new ServerMessage(ServerMessageType.USER_DENY, client), clientAux);
				System.out.println("Client : "+client.getName()+" denny : "+clientAux.getName());
				break;
			case EXIT_CALL:
				finishCall(client);
				System.out.println("Client : "+client.getName()+" saiu da liga��o");
				break;
			default:
				break;
			}
		}
	}
	
	private void sendMessageToAll(ServerMessage serverMessage){
		for (Client client : clientsWithOutCall)
			client.send(serverMessage);
		for (Call call : calls){
			call.getClientCustomer().send(serverMessage);
			call.getClientSupport().send(serverMessage);
		}
	}
	
	private void sendMessageToOne(ServerMessage serverMessage, Client client){
		client.send(serverMessage);
	}
	
	private void createCall(Client clientSupport, Client clientCustomer){
		sendMessageToAll(new ServerMessage(ServerMessageType.HIDE_USER, clientSupport));
		sendMessageToAll(new ServerMessage(ServerMessageType.HIDE_USER, clientCustomer));
		Call call = new Call(clientsWithOutCall.remove(clientsWithOutCall.indexOf(clientSupport)), clientsWithOutCall.remove(clientsWithOutCall.indexOf(clientCustomer)));
		calls.add(call);
	}
	
	private void removeClient(Client client){
		if (clientsWithOutCall.contains(client)){
			clientsWithOutCall.remove(client);
			System.out.println("Clients : "+clientsWithOutCall.toString());
			return;
		}

		Client otherClient = null;
		
		for (int i = 0; i<calls.size();i++){
			if (calls.get(i).getClientCustomer().equals(client)){
				otherClient = calls.remove(i).getClientSupport();
				clientsWithOutCall.add(otherClient);
				sendMessageToAll(new ServerMessage(ServerMessageType.SHOW_USER, otherClient));
				System.out.println("Client : "+otherClient.getName()+" come back to avaiable list");
				break;
			}
			else if (calls.get(i).getClientSupport().equals(client)){
				otherClient = calls.remove(i).getClientCustomer();
				clientsWithOutCall.add(otherClient);
				sendMessageToAll(new ServerMessage(ServerMessageType.SHOW_USER, otherClient));
				System.out.println("Client : "+otherClient.getName()+" come back to avaiable list");
				break;
			}
		}
	}
	
	private void finishCall(Client client){
		for (int i = 0; i<calls.size();i++){
			if (calls.get(i).getClientCustomer().equals(client)||
				calls.get(i).getClientSupport().equals(client)){
				Call callAux = calls.remove(i);
				clientsWithOutCall.add(callAux.getClientCustomer());
				clientsWithOutCall.add(callAux.getClientSupport());
				sendMessageToAll(new ServerMessage(ServerMessageType.SHOW_USER, callAux.getClientCustomer()));
				sendMessageToAll(new ServerMessage(ServerMessageType.SHOW_USER, callAux.getClientSupport()));
				break;
			}
		}
	}
	
	private int getNextId(){
		int topId = 0;
		for (Client client : clientsWithOutCall){
			if (client.getId()>topId)
				topId = client.getId();
		}
		for (Call call : calls){
			if (call.getClientCustomer().getId()>topId)
				topId = call.getClientCustomer().getId();
			if (call.getClientSupport().getId()>topId)
				topId = call.getClientSupport().getId();
		}
		return topId+1;
	}
}
