package model;

import java.util.Observable;
import java.util.Observer;

import model.message.Message;

public class Call implements Observer{

	private Client clientSupport;
	
	private Client clientCustomer;

	public Call() {}
	
	public Call(Client clientSupport, Client clientCustomer) {
		this.clientSupport = clientSupport;
		this.clientCustomer = clientCustomer;
		clientSupport.addObserver(this);
		clientCustomer.addObserver(this);
	}

	public void update(Observable observable, Object object) {
		if (observable instanceof Client){
			Client clientAux = (Client)observable;
			if (clientAux.equals(clientSupport)){
				clientCustomer.send((Message) object);
			}else if (clientAux.equals(clientCustomer)){
				clientSupport.send((Message) object);
			}
		}
	};
	
	public Client getClientSupport() {
		return clientSupport;
	}

	public void setClientSupport(Client clientSupport) {
		this.clientSupport = clientSupport;
	}

	public Client getClientCustomer() {
		return clientCustomer;
	}

	public void setClientCustomer(Client clientCustomer) {
		this.clientCustomer = clientCustomer;
	}

	@Override
	public String toString() {
		return "Call [clientSupport=" + clientSupport + ", clientCustomer=" + clientCustomer + "]";
	}
	
}
