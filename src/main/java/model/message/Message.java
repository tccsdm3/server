package model.message;

import com.google.gson.annotations.Expose;

import model.type.ClientMessageType;

public class Message extends MessageI{

	@Expose
	private ClientMessageType messageType;

	@Expose
	private String value;
	
	public Message() {}

	public Message(ClientMessageType messageType, String value) {
		this.messageType = messageType;
		this.value = value;
	}
	
	public Message(ClientMessageType messageType) {
		this.messageType = messageType;
	}

	public ClientMessageType getMessageType() {
		return messageType;
	}

	public void setMessageType(ClientMessageType messageType) {
		this.messageType = messageType;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
