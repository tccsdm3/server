package model;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Observable;
import java.util.Scanner;

import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.Expose;

import model.message.Message;
import model.message.MessageI;
import model.message.ServerMessage;
import model.type.ClientMessageType;
import util.GsonSingleton;

public class Client extends Observable{
	
	@Expose
	private int id;

	@Expose
	private String name;
	
	private Socket socket;
	
	public Client() {}

	public Client(int id, Socket socket) {
		this.id = id;
		this.name = "User";
		this.socket = socket;
		communicationService.start();
	}
	
	public Client(int id) {
		this.id = id;
		this.name = "User";
	}
	
	private Thread communicationService = new Thread(){

		private Scanner scanner;

		public void run() {
			try {
				scanner = new Scanner(socket.getInputStream());
		        while (scanner.hasNextLine()){
		        	Message message = (Message)GsonSingleton.getInstance().fromJson(scanner.nextLine(), MessageI.class);
		        	if (message.getMessageType() == ClientMessageType.CHANGE_NAME)
		        		name = message.getValue();
		        	Client.this.notify(message);
		        }
			} catch (JsonSyntaxException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
        	Client.this.notify(new Message(ClientMessageType.EXIT));
			deleteObservers();
		}
	};
	
	private void notify(Message message){
		setChanged();
		notifyObservers(message);
	}
	
	public void send(final MessageI message){
		if (message instanceof ServerMessage && 
			((ServerMessage)message).getClient().equals(this))
			return;
		PrintStream printStream;
		try {
			printStream = new PrintStream(socket.getOutputStream());
			printStream.print(GsonSingleton.getInstance().toJson(message, MessageI.class)+System.lineSeparator());
		} catch (IOException e) {e.printStackTrace();}
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Client [id=" + id + ", name=" + name + "]";
	}
	
}
