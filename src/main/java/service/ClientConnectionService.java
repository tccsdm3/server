package service;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Observable;

public class ClientConnectionService extends Observable implements Runnable{

	private ServerSocket serverSocket;

	public ClientConnectionService(ServerSocket serverSocket) {
		this.serverSocket = serverSocket;
	}

	public void run() {
		while (serverSocket != null && !serverSocket.isClosed()){
			try {
				Socket socket = serverSocket.accept();
				sendNewSocket(socket);
			} catch (IOException e) {e.printStackTrace();}
		}
		deleteObservers();
	}
	
	private void sendNewSocket(Socket socket){
		setChanged();
		notifyObservers(socket);
	}
	
}
